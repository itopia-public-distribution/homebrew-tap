# typed: false
# frozen_string_literal: true

class ProtocJsonMeta < Formula
    desc "protoc plugin which exports metadata about the protobuf files in a json format into ./output.json"
    homepage "https://isynth.io"
    version "0.2.2"
    license "proprietary"
  
    # depends_on "helm"
    # depends_on "kubernetes-cli"
  
    on_macos do
      if Hardware::CPU.arm?
        url "https://gitlab.com/api/v4/projects/65768824/packages/generic/protoc-json-meta/0.2.2/protoc-gen-jsonmeta-0.2.2-darwin-arm64"
        sha256 "47928528d16273dfb74a72cc8512a1567970686fbc850261fe1a571234bafcb3"
  
        def install
          bin.install "protoc-gen-jsonmeta-0.2.2-darwin-arm64" => "protoc-gen-jsonmeta"
        end
      end
      if Hardware::CPU.intel?
        url "https://gitlab.com/api/v4/projects/65768824/packages/generic/protoc-json-meta/0.2.2/protoc-gen-jsonmeta-0.2.2-darwin-amd64"
        sha256 "6a38d064ea8b3512bff53d73ca7d09eb7b5cd61b56cffea4684b5062a549fb87"
  
        def install
          bin.install "protoc-gen-jsonmeta-0.2.2-darwin-amd64" => "protoc-gen-jsonmeta"
        end
      end
    end
  
    on_linux do
      if Hardware::CPU.intel?
        url "https://gitlab.com/api/v4/projects/65768824/packages/generic/protoc-json-meta/0.2.2/protoc-gen-jsonmeta-0.2.2-linux-amd64"
        sha256 "ec95120a61b59e11bb5dbe6ac80236c24282ee1d0b82d3e75a16e6c456c686f1"
  
        def install
          bin.install "protoc-gen-jsonmeta-0.2.2-linux-amd64" => "protoc-gen-jsonmeta"
        end
      end
    end
  end