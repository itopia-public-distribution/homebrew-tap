# homebrew-tap


## Prerequisities

Have homebrew installed on your local machine (Mac OS or Linux), cf. [https://brew.sh/](https://brew.sh/) for instructions

## Add the itopia homebrew tap to your local homebrew

Open a shell and execute the following command:

```bash
brew tap --force-auto-update itopia/itools https://gitlab.com/isynth-io/homebrew-tap
```



## Install an itopia tool

Now you can easily install all tools published by itopia in this homebrew tap. For example you can install the isynth CLI with the following command:

```bash 
# on MacOS the --force-auto-update might not always work so you might need to update manualle
brew update
brew install isynth-cli
```

## Update an itopia tool

You can check if a new version of your installed tool has been published:

```bash 
# on MacOS the --force-auto-update might not always work so you might need to update manualle
brew update
# check for new versions 
brew outdated
```

You can update an app (e.g. isynth-cli) by running the following command:

```bash
brew upgrade isynth-cli
```