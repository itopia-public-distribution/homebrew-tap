# typed: false
# frozen_string_literal: true

class Raccoon < Formula
    desc "single binary cli for easy and reliable handling of gitlab releases - provided by Raymond the Raccoon"
    homepage "https://isynth.io"
    version "0.9.3"
    license "proprietary"
  
    # depends_on "helm"
    # depends_on "kubernetes-cli"
  
    on_macos do
      if Hardware::CPU.arm?
        url "https://gitlab.com/api/v4/projects/58009864/packages/generic/raccoon/0.9.3/raccoon-0.9.3-darwin-arm64"
        sha256 "3e7fb800a6b646e667b1bf73fe4a02e6bfa3b721096c98a782c274a8e7125070"
  
        def install
          bin.install "raccoon-0.9.3-darwin-arm64" => "raccoon"
        end
      end
      if Hardware::CPU.intel?
        url "https://gitlab.com/api/v4/projects/58009864/packages/generic/raccoon/0.9.3/raccoon-0.9.3-darwin-amd64"
        sha256 "929428bceed4d3e07b3a94c8a45631c351a50fcc0d18761515b81dd4844f1cc8"
  
        def install
          bin.install "raccoon-0.9.3-darwin-amd64" => "raccoon"
        end
      end
    end
  
    on_linux do
    #   if Hardware::CPU.arm? && Hardware::CPU.is_64_bit?
    #     url "https://gitlab.com/api/v4/projects/54149759/packages/generic/isynth-cli/v1.11.0/isynth-cli-v1.11.0-linux"
    #     sha256 "70a3b186841a9592616bbfda3699e0b6f158a574719fbb50efa02d33afad281e"
  
    #     def install
    #       bin.install "isynth-cli"
    #     end
    #   end
      if Hardware::CPU.intel?
        url "https://gitlab.com/api/v4/projects/58009864/packages/generic/raccoon/0.9.3/raccoon-0.9.3-linux-amd64"
        sha256 "630e80ad557d79c23df7cc899f91ba46256ab26e9f7c613718d68ec4997bb303"
  
        def install
          bin.install "raccoon-0.9.3-linux-amd64" => "raccoon"
        end
      end
    end
  end