# typed: false
# frozen_string_literal: true

class IsynthCli < Formula
    desc "single binary cli for remote control of the best tool for synthetic data"
    homepage "https://isynth.io"
    version "1.18.4"
    license "proprietary"
  
    # depends_on "helm"
    # depends_on "kubernetes-cli"
  
    on_macos do
      if Hardware::CPU.arm?
        url "https://gitlab.com/api/v4/projects/54272649/packages/generic/isynth-cli/v1.18.4/isynth-cli-v1.18.4-darwin-arm64"
        sha256 "0b5f9cdb5ef6b056c4369c4d454edc1f9ba5b1168ad759e721bf06b431871afa"
  
        def install
          bin.install "isynth-cli-v1.18.4-darwin-arm64" => "isynth-cli"
        end
      end
      if Hardware::CPU.intel?
        url "https://gitlab.com/api/v4/projects/54272649/packages/generic/isynth-cli/v1.18.4/isynth-cli-v1.18.4-darwin-amd64"
        sha256 "f3a81d93d955374eee823695d43f49a8622b52b7e5e756b90c4cd8230c4c1c82"
  
        def install
          bin.install "isynth-cli-v1.18.4-darwin-amd64" => "isynth-cli"
        end
      end
    end
  
    on_linux do
    #   if Hardware::CPU.arm? && Hardware::CPU.is_64_bit?
    #     url "https://gitlab.com/api/v4/projects/54149759/packages/generic/isynth-cli/v1.11.0/isynth-cli-v1.11.0-linux"
    #     sha256 "70a3b186841a9592616bbfda3699e0b6f158a574719fbb50efa02d33afad281e"
  
    #     def install
    #       bin.install "isynth-cli"
    #     end
    #   end
    # homebrew on linux does not support ARM
      if Hardware::CPU.intel?
        url "https://gitlab.com/api/v4/projects/54272649/packages/generic/isynth-cli/v1.18.4/isynth-cli-v1.18.4-linux-amd64"
        sha256 "e23353c47547af652343423e397020b4cee0dda349831c30d0035ce29ab7f411"
  
        def install
          bin.install "isynth-cli-v1.18.4-linux-amd64" => "isynth-cli"
        end
      end
    end
  end